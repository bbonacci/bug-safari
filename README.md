<!--
SPDX-FileCopyrightText: 2023 Ben Bonacci <[myfirstname] at [myfullname] dot com>
SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL
-->

# Bug Safari

## About
Bug Safari is a simple script that queries a Bugzilla instance for bug statistics and sends a report to a room on Matrix.
Specifically, this project was created in spirit of the KDE goal [Automate and Systematise Internal Processes](https://community.kde.org/Goals/Automate_and_systematize_internal_processes#Chat).

## How it works
Bugzilla has a REST API that can give out a JSON response for bugs in a bug list. The URL structure is almost identical to accessing a bug list normally  with ```/rest/bug?``` added at the start of the URL before the actual list query. For example,

```https://bugs.kde.org**/buglist.cgi**?chfield=%5BBug%20creation%5D&chfieldfrom=24h # Normal list URL```

```https://bugs.kde.org**/rest/bug**?chfield=%5BBug%20creation%5D&chfieldfrom=24h # JSON list URL```

All these list URLs are stored in a JSON array called bug_lists.json that the script will use to download each list as a JSON and store it in the [temp](temp) folder. The bugs in each list and any deltas is calculated within Python and the results are then sent to Matrix over HTTP.

## Install
1. Clone the repository ```git clone git@invent.kde.org:bbonacci/bug-safari.git && cd $_```
2. Create and enter a Python environment ```python3 -m venv .env && source .env/bin/activate```
3. Install the requirements ```pip install -r requirements.txt```
4. Copy and enter secrets and configuration ```cp secrets_example.json secrets.json && kate secrets.json```
5. Copy and enter bug lists for reporting ```cp bug_lists_example.json bug_lists.json && kate bug_lists.json```
6. Run the script ```python3 safari.py```

To get regular reports, the script can be set to run under a cron job.

```0 0 * * * cd /path/to/bug-safari && python3 /path/to/bug-safari/safari.py```

If you do not have a MTA installed on the machine, you can export the output to a text file.

```0 0 * * * cd /path/to/bug-safari && python3 /path/to/bug-safari/safari.py >> /path/to/bug-safari.log 2>&1```

## Development
1. Clone the repository ```git clone git@invent.kde.org:bbonacci/bug-safari.git && cd $_```
2. Create and switch to a new branch ```git branch patch-xxxx && git checkout patch-xxxx```
3. Make and test your changes
4. Commit your changes to ```patch-xxxx``` and open a merge request
