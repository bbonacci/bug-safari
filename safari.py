#!/usr/bin/env python

""""""
# Bug Safari - A simple script that queries Bugzilla for Plasma bug statistics and sends a report to the #plasma:kde.org room on Matrix."""

# SPDX-FileCopyrightText: 2023 Ben Bonacci <[myfirstname] at [myfullname] dot com>
# SPDX-FileCopyrightText: 2024 Martin Riethmayer <ripper at freakmail dot com>
# SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

""""""

import json
import os
import requests
from time import sleep
from random import randint

# Load the json file with the Bugzilla and Matrix secrets
with open('secrets.json') as f:
	secrets = json.load(f)
	print("Loaded secrets.json")

# Load the json file with the Bugzilla lists to report on
with open('bug_lists.json') as f:
	lists = json.load(f)
	print("Loaded bug_lists.json")

# Load the json file with the Matrix rooms to send reports to
#with open('chat_rooms.json') as f:
	#rooms = json.load(f)
	#print("Loaded chat_rooms.json")

# Prepare secrets
conf_safari_name = secrets["conf_safari_name"]
conf_safari_debug = secrets["conf_safari_debug"]
conf_safari_delta = secrets["conf_safari_delta"]
conf_matrix_homeserver = secrets["conf_matrix_homeserver"]
conf_matrix_room = secrets["conf_matrix_room"]

bugzillaToken = secrets["bugzilla_token"]
matrixToken = secrets["matrix_token"]
print("Secrets ready")

# Prepare headers
bugzillaHeader = {"User-Agent": "bug-safari (" + conf_safari_name + ") 1.1.0", "X-BUGZILLA-API-KEY": bugzillaToken}
matrixHeader = {"User-Agent": "bug-safari (" + conf_safari_name + ") 1.1.0", "Authorization": "Bearer " + matrixToken}
print("Headers ready")

# Use Bugzilla's API to check status of bug lists
print("Downloading bug lists...")

i = 1
for list in lists.values():
 	try:
 		if conf_safari_delta == True and os.path.isfile("temp/" + str(i) + ".json"):
 			os.rename("temp/" + str(i) + ".json", "temp/" + str(i) + "_last.json")
 			pass
 		with open("temp/" + str(i) + ".json", 'w') as f:
 			json.dump(requests.get(list, headers=bugzillaHeader).json(), f)
 		f.close()
 		i = i + 1
 		sleep(randint(1, 5))
 	except Exception as e:
 		print(e)
 		exit(1)

# Use bug lists to format Matrix message body
matrixBody = "<b>Today's " + conf_safari_name + " bug report:</b>"

i = 1
for listName, listUrl in lists.items():
	try:
		with open("temp/" + str(i) + ".json") as f:
			curReport = json.load(f)
		matrixBody += "<br/><a href='" + listUrl.replace("/rest/bug", "/buglist.cgi") + "'>" + str(len(curReport["bugs"])) + " " + listName + "</a>"
		if conf_safari_delta == True and os.path.isfile("temp/" + str(i) + "_last.json"):
			with open("temp/" + str(i) + "_last.json") as f:
				oldReport = json.load(f)
			
			if oldReport != curReport:
				oldBugsLen = len(oldReport["bugs"])
				oldBugs = []
				if oldBugsLen > 0:
					for oldBug in oldReport["bugs"]:
						oldBugs.append(oldBug["id"])
				
				curBugsLen = len(curReport["bugs"])
				curBugs = []
				if curBugsLen > 0:
					for curBug in curReport["bugs"]:
						curBugs.append(curBug["id"])

				bugsClosed = 0
				bugsClosedMessage = ""
				for oldBug in oldBugs:
					if oldBug not in curBugs:
						if bugsClosedMessage != "":
							bugsClosedMessage += ", "
						bugsClosed += 1
						bugsClosedMessage += "<a href='https://bugs.kde.org/show_bug.cgi?id=" + str(oldBug) + "'><del>" + str(oldBug) + "</del></a>"
				
				if bugsClosed > 0:
					matrixBody += ", " + str(bugsClosed) + " bug(s) fixed: " + bugsClosedMessage

				bugsNew = 0
				bugsNewMessage = ""
				for curBug in curBugs:
					if curBug not in oldBugs:
						if bugsNewMessage != "":
							bugsNewMessage += ", "
						bugsNew += 1
						bugsNewMessage += "<a href='https://bugs.kde.org/show_bug.cgi?id=" + str(curBug) + "'><ins>" + str(curBug) + "</ins></a>"
				
				if bugsNew > 0:
					matrixBody += ", " + str(bugsNew) + " new bug(s): " + bugsNewMessage
						
			else:
				matrixBody += " (no change since last report)"
		i = i + 1
	except Exception as e:
		print(e)
		exit(1)

matrixBody += "<br/><b>See you tomorrow for the next report!</b>"
if conf_safari_debug == True:
	print("Debug enabled, not sending. If conf_safari_debug would be false, we would now send:")
	print(matrixBody)
else:
	print("Sending to Matrix...")
	# Use Matrix's API to send bug status report to the room
	matrixJson = {"body": "", "format": "org.matrix.custom.html", "formatted_body": matrixBody, "msgtype": "m.notice"}
	requests.post(conf_matrix_homeserver + "/_matrix/client/r0/rooms/" + conf_matrix_room + "/send/m.room.message", headers=matrixHeader, json=matrixJson)

# Finish
print("Done!")
